import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { LowercasePipe } from './lowercase.pipe';
import { UppercasePipe } from '../uppercase.pipe';



@NgModule({
  declarations: [LowercasePipe, UppercasePipe],
  imports: [
    CommonModule
  ]
})
export class AwsomeModule { }
